package com.example.restservice;

import java.util.concurrent.atomic.AtomicLong;
import io.sentry.Sentry;
import java.lang.Exception;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.sentry.ITransaction;
import io.sentry.SentryTraceHeader;
import io.sentry.TransactionContext;
import io.sentry.exception.InvalidSentryTraceHeaderException;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.Thread;
@RestController
public class GreetingController {
	

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "User") String name, HttpServletRequest request, 
        HttpServletResponse response) {
		return new Greeting(counter.incrementAndGet(), String.format(template, name),request);
	}
}