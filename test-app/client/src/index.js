import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as Sentry from "@sentry/react";
import { Integrations  } from "@sentry/tracing";
import App from './App';
import reportWebVitals from './reportWebVitals';
Sentry.init({
  dsn: "https://8aa12ea752e141fdbf51a2efb2c20ffe@o918224.ingest.sentry.io/5861551",
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0 ,// to be ajdusted according to the need of data to be send
});
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
