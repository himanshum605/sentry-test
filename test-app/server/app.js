const express=require("express");
const app=express();
const https=require("http");
const Sentry = require("@sentry/node");
const SentryTracing = require("@sentry/tracing");
const bodyParser=require("body-parser");
const cors=require('cors');
const axios = require('axios');
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json())
app.use(cors());
Sentry.init({
    dsn: "https://fa0c39641c6c4e9d97d958220577f91f@o918224.ingest.sentry.io/5861558",
    integrations: [
      // enable HTTP calls tracing
      new Sentry.Integrations.Http({ tracing: true }),
      // enable Express.js middleware tracing
      new SentryTracing.Integrations.Express({ app }),
    ],
    tracesSampleRate: 1.0, // to be ajdusted according to the need of data to be send
  });
  
  app.use(Sentry.Handlers.requestHandler());
  // TracingHandler creates a trace for every incoming request
  app.use(Sentry.Handlers.tracingHandler());

app.post("/",(req,res)=>{
    const name=req.body.userName;
    const url="http://localhost:8080/greeting?name="+name+"";
    // https.get(url,(response)=>{
    //     response.on("data",(data)=>{
    //         const userData=JSON.parse(data);
    //         const userId=userData.id;
    //         const content=userData.content;
    //         console.log()
    //         res.write("<p>The entry id is "+ userId+" <p>");
    //         res.write("<h1>"+ content+ "</h1>");
    //         res.send();
    //     })
    // })
    
    axios.get(url)
    .then(response=>{
        const userId=response.data.id;
        const content=response.data.content;
      
        res.send({id:userId,
        con:content});
    })

})
app.listen(5000,function(){
    console.log("server is running on 5000");
})
