1) Running the express server: 
    Set the current directory to the 'server' folder in 'test-app' 
    Run the command "nodemon app.js" in the terminal to start the express server. 
2) Starting Spring server 
    Set the current directory to 'rest-service' 
    Run the command "mvnw spring-boot:run" in the terminal to start the Spring server. 
3) Starting the cliend server 
    Change the current directory to the 'client' folder in 'test-app' 
    Run the command "npm start" in the terminal