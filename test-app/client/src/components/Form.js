import React,{useState,useEffect} from 'react';
import axios from 'axios';
const Form=()=>{
    const [enteredName,setEnteredName]=useState('');
    const [enteredid,setEnteredId]=useState(' ');
    const [enteredcontent,setEnteredContent]=useState('');
    const [distu,setDis]=useState(1);
    const namechanger=(event)=>{
        setEnteredName(event.target.value);
    }
    const setdisone=()=>{
        setDis(0);
    }
    const formpost=(event)=>{
        axios.post('http://localhost:5000/',{
            userName:enteredName
        })
        .then((response)=>{
            setEnteredId(<p>The id is {response.data.id}</p>);
            setEnteredContent(<p> {response.data.con} </p>)
            setDis(1);
            console.log(typeof(distu));
        })
        .catch((error)=>{
            console.log(error);
        })
           
    }
    useEffect(()=>{
            formpost();
    },[])
       
            if(distu===0)
            {return(
                <form >
    <label for="name">Enter your Name</label>
    <input id="name" type="text" name="name" onChange={namechanger}/>
    <button type="button" onClick={formpost}> Submit</button>
 </form>
            )
}
else
{
    return(
 <div>
 <h1>{enteredid}</h1>
 <h2>{enteredcontent}</h2>
 <button type="submit" onClick={setdisone}>Another User</button>
      </div>
    )
}   
};
export default Form;