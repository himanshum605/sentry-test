package com.example.restservice;
import io.sentry.ITransaction;
import io.sentry.Sentry;
import io.sentry.SentryTraceHeader;
import io.sentry.TransactionContext;
import io.sentry.exception.InvalidSentryTraceHeaderException;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import io.sentry.SpanStatus;
public class Greeting {
	private final long id;
	private final String content;
	public Greeting(long id, String content,HttpServletRequest request) {
		Sentry.init(options -> {
  options.setDsn("https://72c6faf63dac484da801c24317aefe57@o918224.ingest.sentry.io/5861563");// to be ajdusted according to the need of data to be send
  options.setTracesSampleRate(1.0);
  options.setDebug(true);
});
String sentryTrace = request.getHeader(SentryTraceHeader.SENTRY_TRACE_HEADER);
		
ITransaction transaction = null;
try {
  transaction = Sentry.startTransaction(TransactionContext.fromSentryTrace("name", "op", new SentryTraceHeader(sentryTrace)));
  transaction.finish();
  
} catch (InvalidSentryTraceHeaderException e) {
	
}

		this.id = id;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public String getContent() {
		return content;
	}
}